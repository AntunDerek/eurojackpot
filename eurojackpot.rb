class Eurojackpot
    attr_reader :jackpot
    @start, @games, @finish = nil
    YEAR_WEEKS = 52

    def initialize(jackpot)
        self.validate(jackpot)
        @jackpot = self.sort_jackpot(jackpot)
    end

    def validate(jackpot)
        raise "Not an array of numbers" unless jackpot.is_a?(Array) && jackpot.all? {|n| n.is_a?(Integer)}

        raise "Accepting only an array with 7 numbers" unless jackpot.size == 7

        raise "Numbers are not within the range of 1 to 50" unless jackpot[0..4].all? {|n| n.between?(1, 50)}
        raise "Euronumbers are not within the range of 1 to 10" unless jackpot[-2..-1].all? {|n| n.between?(1, 10)}

        raise "Numbers are not unique" unless jackpot[0..4].uniq == jackpot[0..4]
        raise "Euronumbers are not unique" unless jackpot[-2..-1].uniq == jackpot[-2..-1]
    end

    def sort_jackpot(jackpot)
        numbers = jackpot[0..4].sort
        euronumbers = jackpot[-2..-1].sort
        numbers + euronumbers
    end

    def create_jackpot
        (1..50).to_a.sample(5).sort + (1..10).to_a.sample(2).sort
    end

    def jackpot
        @start = Time.now
        @games = 0
        user_jackpot_number = nil

        until user_jackpot_number == @jackpot do
            @games += 1
            user_jackpot_number = self.create_jackpot 
            print("Games: #{@games}\r")
        end   
        @finish = Time.now
        self.jackpot_print_results
    end

    def jackpot_print_results
        puts "Games: #{@games}"
        puts "Years: #{@games / YEAR_WEEKS}"
        puts "Money spent: #{@games * 15}kn"
        puts "Execution time: #{@finish - @start}s"
    end
end

jackpot = Eurojackpot.new([1, 5, 33, 40, 27, 2, 1]).jackpot